import React, { Component } from 'react';
import ThemeProvider from '@ddl/react-theme-provider';
import { PrimaryButton } from '@ddl/react-button';
import Icon from '@ddl/react-icon';
import { ForwardLinkGlyph, AccountGlyph } from '@ddl/react-assets';
import StandaloneLink from '@ddl/react-link';
require('./style.scss');



const ValueStrip = ({ subtitle, customIcon, inverse }) => (
	<div>
		<StandaloneLink id="stripLink" href={"#"} className={"ddl-value-strip" + (inverse ? ' inverse': '') }>
			<div>
				{ customIcon ? <div className={ "ddl-value-strip_custom-icon" + (subtitle ? ' ddl-value-strip_content-top':'') }>
					<Icon id="stripIcon" className="ddl-round-icon-bg" inverse={ true } size="md">
						<AccountGlyph />
					</Icon>
				</div> : ''}
				<div className={ "ddl-value-strip_content" + (subtitle ? ' ddl-value-strip_content-top':'')}>
					<h3 className="ddl-value-strip_title">Title</h3>
					{ subtitle && 
						<p className="ddl-value-strip_sub-title">subtitle</p>
					}
				</div>
				<div className="ddl-value-strip_text">&pound; 0.00</div>

				<div className="ddl-value-strip_forward-link" id="next_icon">
					<Icon id="voucher_icon" className="ddl-round-icon-bg" inverse={true} size="md">
						<ForwardLinkGlyph />
					</Icon>
					
				</div>
			</div>
		</StandaloneLink>
		

		
	</div>
);

export default ValueStrip;