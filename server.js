var express = require('express');
var app = express();
var port = 9090;

app.use(express.static(`./build/`));

app.get('/',(req,res) => {
	res.sendFile('index.html');
});
app.listen(port ,() => {
	console.log('listening');
});