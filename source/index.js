import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ThemeProvider from '@ddl/react-theme-provider';
// import Footer from './component/footer';
import ValueStrip from './component/valueStrip';

require('./style.scss');

class Body extends Component {
	render() {
		return (
			<ThemeProvider>
				<div style={ { clear: 'both' } }>
					<div style={ { float: 'left', width: '45%' } }>
						<ValueStrip subtitle={ true } customIcon={ true } inverse={ true } />
					</div>
					<div style={ { float: 'right', width: '45%' } }>
						<ValueStrip subtitle={ true } customIcon={ true } inverse={ false } />
					</div>
				</div>
				<div style={ { clear: 'both' } }>
					<div style={ { float: 'left', width: '45%' } }>
						<ValueStrip subtitle={ false } customIcon={ true } inverse={ true } />
					</div>
					<div style={ { float: 'right', width: '45%' } }>
						<ValueStrip subtitle={ false } customIcon={ true } inverse={ false } />
					</div>
				</div>
				<div style={ { clear: 'both' } }>
					<div style={ { float: 'left', width: '45%' } }>
						<ValueStrip subtitle={ false } customIcon={ false } inverse={ true } />
					</div>
					<div style={ { float: 'right', width: '45%' } }>
						<ValueStrip subtitle={ false } customIcon={ false } inverse={ false } />
					</div>
				</div>
				<div style={ { clear: 'both' } }>
					<div style={ { float: 'left', width: '45%' } }>
						<ValueStrip subtitle={ true } customIcon={ false } inverse={ true } />
					</div>
					<div style={ { float: 'right', width: '45%' } }>
						<ValueStrip subtitle={ true } customIcon={ false } inverse={ false } />
						</div>
				</div>
			</ThemeProvider>

					)
	}
}
ReactDOM.render(<Body />, document.getElementById('some-app'));