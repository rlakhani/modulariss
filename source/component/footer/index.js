import React, { Component } from 'react';

import './style.scss';

export default class Footer extends Component {
	render() {
		return (
			<div className="footer">
				<h2>Title</h2>
				<ul>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
				</ul>
				<ul>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
				</ul>
				<ul>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
					<li>
						<a href="#">link</a>
					</li>
				</ul>
			</div>
		)
	}
}