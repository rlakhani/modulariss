import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import ValueStrip from './index.js';

describe('valueSrip', () => {
	it('show the strip', () => {
		const wrapper = shallow(<ValueStrip />);
		let title = wrapper.find('.title');
		let amount = wrapper.find('.amount');
		expect(wrapper.find('#voucher_icon').first().props().className).to.be.equal('voucher-icon');
		expect(wrapper.find('#next_icon').last().props().className).to.be.equal('go-arrow');
		expect(title.text()).to.equal('Title');
		expect(amount.text()).to.equal('£ 0.00');
	});
});